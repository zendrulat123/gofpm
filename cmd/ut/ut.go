package com

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
)

//file logger
func FInfo(f os.FileInfo) {

	fmt.Println("File Name:", f.Name())        // Base name of the file
	fmt.Println("Size:", f.Size())             // Length in bytes for regular files
	fmt.Println("Permissions:", f.Mode())      // File mode bits
	fmt.Println("Last Modified:", f.ModTime()) // Last modification time
	fmt.Println("Is Directory: ", f.IsDir())   // Abbreviation for Mode().IsDir()
}

func Watch(dir string) {

	files, _ := ioutil.ReadDir(dir)

	var newestFile string
	var newestTime int64 = 0
	for _, f := range files {
		fi, err := os.Stat(dir + f.Name())
		if err != nil {
			fmt.Println(err)
		}
		currTime := fi.ModTime().Unix()
		if currTime > newestTime {
			newestTime = currTime
			newestFile = f.Name()
		}
	}
	fmt.Println("~~~~~~~~~~~~~~~", newestFile)
	curr_wd, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	err, out, errout := Shellout(`cd ` + curr_wd + `&&  cd ` + dir + ` && GOARCH=wasm GOOS=js go build -o lib.wasm main.go && cp "$(go env GOROOT)/misc/wasm/wasm_exec.js" .  && cd  ../../  && go build -race  && pwd`)
	if err != nil {
		log.Printf("error: %v\n", err)
	}
	fmt.Println(filepath.Base(curr_wd))
	fmt.Println(out)
	fmt.Println("--- errs ---")
	fmt.Println(errout)

}
func Shellout(command string) (error, string, string) {
	var stdout bytes.Buffer
	var stderr bytes.Buffer
	cmd := exec.Command("bash", "-c", command)
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	return err, stdout.String(), stderr.String()
}
