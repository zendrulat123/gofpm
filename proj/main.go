package main

import (
	"fmt"
	"github.com/slayer/autorestart"
	"log"
	"net/http"
	//"os"
	//"syscall"
	"text/template"
	"time"
)

func main() {
	autorestart.WatchPeriod = 3 * time.Second
	// custom file to watch
	autorestart.WatchFilename = "static/wasm/main.go"

	// Notifier
	restart := autorestart.GetNotifier()
	go func() {
		<-restart
		log.Printf("I will restart shortly")
	}()
	http.HandleFunc("/home", home)
	http.HandleFunc("homess", homes)
	// add route to serve pictures
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))
	autorestart.StartWatcher()
	log.Fatal(http.ListenAndServe(":8082", nil))
}

var tpl *template.Template

func init() {
	fmt.Println("starting...")
	tpl = template.Must(template.ParseGlob("templates/*.html"))
}
func home(w http.ResponseWriter, r *http.Request) {
	fmt.Println("starting home...")
	tpl.ExecuteTemplate(w, "home.html", nil)
}
func homes(w http.ResponseWriter, r *http.Request) {
	fmt.Println("starting homess...")
	tpl.ExecuteTemplate(w, "homes.html", nil)
}
